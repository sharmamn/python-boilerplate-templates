#!/usr/bin/env python
"""
Command-Line Interface

    # Get help output
    ./gen.py -h

    # Generate boilerplate from a specific config (for tests during development)
    ./gen.py py2+pytest+tox

    # Generate boilerplate from a specific config in a specific directory (eg. `./tmp/`)
    ./gen.py py2+pytest+tox -o tmp/

    # Generate boilerplate from a specific config and test it (run main.py)
    ./gen.py py2+pytest+tox -t

    # Generate all boilerplates
    ./gen.py all

    # Generate all boilerplates and test em all
    ./gen.py all -t
"""

__author__ = "Chris Hager"
__version__ = "1.0.0"
__license__ = "MIT"

import os
import shutil
import argparse
import subprocess
import logging
import json

from logger import setup_logger
from generator import generate_boilerplate_files, generate_boilerplate_zip
from generator import get_possible_config_combinations, TEST_SPECIAL_COMMAND

logger = setup_logger(level=logging.INFO)

dir_script = os.path.abspath(os.path.dirname(__file__))
dir_output_base = os.path.abspath(os.path.join(dir_script, "..", "generated-boilerplates"))


class TestFailedError(Exception):
    """ Error raised when execution of boilerplate fails """
    pass


def generate(config, production=False, dir_output=None):
    """
    Generate boilerplate for a specific configuration, save the files,
    and when `production == True` also save `index.json` and `boilerplate.zip`
    """
    logger.debug("Generate config: %s (prod: %s, outdir: %s)", config, production, dir_output)
    dir_bp = dir_output or os.path.join(dir_output_base, config)

    if os.path.exists(dir_bp):
        # Don't delete output directory if it is custom, to prevent wiping the disk.
        if dir_output:
            logger.debug("Custom output directory %s already exists", dir_output)
        else:
            logger.debug("Removing directory %s", dir_bp)
            shutil.rmtree(dir_bp)

    # Create directory
    if not os.path.exists(dir_bp):
        os.mkdir(dir_bp)

    # Build the boilerplate
    config_arr = config.split("+")
    boilerplates_result = generate_boilerplate_files(config_arr)

    # Write to disk
    logger.debug("Writing files:")
    for bp in boilerplates_result:
        filename = os.path.join(dir_bp, bp["name"])
        with open(filename, "w") as f:
            f.write(str(bp["content"]))
            logger.debug("- %s", filename)

    if not production:
        return dir_bp

    files, zip_fn, md5 = generate_boilerplate_zip(config.split("+"), dir_bp, "boilerplate.zip")
    logger.debug("Saved zip file: %s", zip_fn)

    fn_json = os.path.join(dir_bp, "index.json")
    output_json = json.dumps({
        "files": boilerplates_result
    })
    with open(fn_json, "w") as f:
        f.write(output_json)
    logger.debug("Saved %s", fn_json)

    return dir_bp


def run_tests(config, dir_output=None):
    """
    Run the test for a specific configuration (with
    `python2/3 main.py dummyarg`).

    Boilerplate needs to have been generated before with `generate(..)`
    """
    logger.info("Running tests for config: %s", config)
    dir_bp = dir_output or os.path.join(dir_output_base, config)
    config_arr = config.split("+")

    # Check if config contains untestable items
    test_cmd = None
    for key in TEST_SPECIAL_COMMAND:
        if key in config_arr:
            test_cmd = TEST_SPECIAL_COMMAND[key]

    # Helper
    if "py2" in config_arr:
        interpreter = "python"
        if os.getenv("ONLY_TEST_PY3"):
            logger.warn("Skipping tests for Python 2")
            return
    else:
        interpreter = "python3"
        if os.getenv("ONLY_TEST_PY2"):
            logger.warn("Skipping tests for Python 3")
            return

    # Default test command
    if not test_cmd:
        test_cmd = "%s main.py dummyarg" % interpreter

    # Execute primary test
    run_test_command(test_cmd, dir_bp, config_arr)

    # Execute test suite if exists
    if "pytest" in config_arr:
        test_cmd = "pytest"
        run_test_command(test_cmd, dir_bp, config_arr)
    elif "unittest" in config_arr:
        # test_cmd = "%s tests.py" % interpreter
        test_cmd = "python tests.py"
        run_test_command(test_cmd, dir_bp, config_arr)

def run_test_command(cmd, dir_bp, config_arr):
    try:
        logger.debug("Test command: %s", cmd)
        os.chdir(dir_bp)
        o = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
        logger.debug("Test output: %s", o)
    except subprocess.CalledProcessError as e:
        error_info = "executing main.py failed with exit code %s for %s" % (e.returncode, "+".join(config_arr))
        logger.error(error_info)
        logger.error(e.output)
        raise TestFailedError("executing main.py failed with exit code %s for %s" % (e.returncode, "+".join(config_arr)))


def main(args):
    """ Main entry point of the app """
    logger.info("Boilerplate config: %s", args.config)
    possible_configurations = get_possible_config_combinations()

    if args.config == "all":
        num_total = len(possible_configurations)
        logger.info("%s configurations", num_total)

        for i, config_arr in enumerate(possible_configurations):
            config = "+".join(config_arr)
            logger.info("%s/%s: %s", i+1, num_total, config)

            # Always need to generate
            generate(config, args.production, args.output_directory)

            # Perhaps also run the test on it
            if args.run_tests:
                run_tests(config)

    else:
        # We have a specific config. Validate it first.
        validate_config = sorted(args.config.split("+"))
        valid = False
        for possible_config in possible_configurations:
            if validate_config == sorted(possible_config):
                valid = True
                break
        if not valid:
            logger.error("Invalid config '%s'", args.config)
            exit(1)

        # Okay this is a valid config. Generate now.
        dir_bp = generate(args.config, args.production, args.output_directory)
        logger.info("Generated boilerplate in: %s", dir_bp)

        # Perhaps also run the test on it
        if args.run_tests:
            run_tests(args.config, args.output_directory)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Required positional argument
    parser.add_argument("config", help="(eg. 'py3+pytest+tox', or 'all')")

    # Optional argument flag which defaults to False
    parser.add_argument('-o', '--out-dir', action="store", dest="output_directory")
    parser.add_argument('-t', '--run-tests', action="store_true", default=False)
    parser.add_argument('-v', '--verbose', action="store_true", default=False)

    # Prod settings: zip and JSON
    parser.add_argument(
        '-p',
        '--production',
        action="store_true",
        default=False,
        help="also generate zip file and index.json")

    # Specify output of '--version'
    parser.add_argument(
        '--version',
        action='version',
        version='%(prog)s (version {version})'.format(version=__version__))

    args = parser.parse_args()
    if args.verbose:
        logger = setup_logger(level=logging.DEBUG)

    main(args)
