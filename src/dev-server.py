#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This lightweight Flask dev server produces the boilerplates and zip files
on-the-fly, so you don't have to prerender them when working with the
web frontend.
"""
import os
from flask import Flask, jsonify, send_file
from flask_cors import CORS

from generator import generate_boilerplate_files, generate_boilerplate_zip


app = Flask(__name__)
app.debug = True
CORS(app)


@app.route('/v2/boilerplate/<config>/index.json')
def get_boilerplate_json(config):
    config_arr = config.split("+")
    boilerplates_result = generate_boilerplate_files(config_arr)
    res = jsonify({ "files": boilerplates_result })
    return res


@app.route('/v2/boilerplate/<config>/boilerplate.zip')
def get_boilerplate_zip(config):
    config_arr = config.split("+")
    files, zip_fn, md5 = generate_boilerplate_zip(config_arr)
    return send_file(zip_fn, attachment_filename='boilerplate.zip')


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8000))
    app.run(host='0.0.0.0', port=port)
