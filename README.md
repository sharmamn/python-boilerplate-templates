# python-boilerplate templates

Templates for the boilerplates of [python-boilerplate.com](https://www.python-boilerplate.com).

[![build status](https://gitlab.com/metachris/python-boilerplate-templates/badges/master/build.svg)](https://gitlab.com/metachris/python-boilerplate-templates/commits/master)

Boilerplates are defined in Jinja2 syntax and auto-formatted with YAPF.


## Dependencies

### Python

* [Jinja2](http://jinja.pocoo.org/docs/2.9/)
* [yapf](https://github.com/google/yapf)
* [Flask](http://flask.pocoo.org/docs/latest/) for the dev server

Install them with `pip install -r src/requirements.txt`.

### System

* 7-zip (`apt-get install p7zip-full`)

## Getting Started

You can test the boilerplate generation with the `gen.py` script:

    ./gen.py -h

    # Generate and test all possible combinations
    ./gen.py all -t

    # Generate one specific boilerplate (useful for testing new additions)
    # `./gen.py {config}`
    ./gen.py py2+argparse+unittest+tox

The simple Flask dev server (`dev-server.py`) is useful for working on the web frontend. Rather
than prerendering all the boilerplates it will generate them on demand.


## Contributing

If you have ideas, problems or feedback, please open an issue.
If you send a pull request, it will be happily reviewed ;)


### How to update existing boilerplates

1. Make your changes to the respective template files in `templates/`
1. Generate the updated boilerplates with `./gen.py {your-config}`
1. When your development is done:
  1. Re-generate and test all boilerplates: `./gen.py all -t`
  1. Review the changes and submit a pull request! <3


### How to add a new boilerplate

Let's say you want to add a boilerplate for a new boilerplate, eg. Flask.

This are the steps:

1. Give your addon a short name (eg. `flask`). This will also be used in the URL slugs.
1. Define a constant for your addon on top of `generator.py` using the short name as value, and add the new constant to `CONFIG_FLAGS`.
1. Also add the new addon as a boolean to the top of `generate_boilerplate_files(..)`, and to `template_context` below.
1. Update the templates in `templates/`, modify existings ones and/or add new ones as needed.
1. If the addon adds new template files, add the rendering step to `generate_boilerplate_files(..)` in `generator.py`
1. Generate the boilerplates with `./gen.py {your-config}`, using your addons short name. You can also add the `-t` flag to run the test on it.
1. When your development is done:
  1. Re-generate and test all boilerplates: `./gen.py all -t`
  1. Review the changes and submit a pull request! <3

Additional Info:

* New addons must be available for both Python 2 and 3. Sometimes you can use the same template code, sometimes you will need to write it separately. Prefer one file for both versions.
* New addons are automatically combined with all other possible configurations. So there will
be `py2-flask`, `py2-pytest-flask`, etc.


#### Web frontend of python-boilerplate.com

If you add a new boilerplate flag, of course we want to integrate it into the web frontend.
Please take a look at the [documentation in the web frontend readme](https://gitlab.com/metachris/python-boilerplate-frontend#contributing).


## Author

Chris Hager <chris@linuxuser.at>


## License

MIT
